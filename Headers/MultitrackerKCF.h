#ifndef MULTITRACKERKCF_H
#define MULTITRACKERKCF_H

#include <thread>

#include <opencv2/opencv.hpp>
#include <opencv2/imgproc.hpp>
#include <opencv2/imgcodecs.hpp>
#include <opencv2/tracking.hpp>

#include "yolo_v2_class.hpp"

enum TrackedObjects{Person, Bicycle, Car, Motorbike, Aeroplane, Bus, Train, Truck, Boat};

class MultitrackerKCF
{
  //--> ********** tracker options *********
private:
    std::vector<cv::Ptr<cv::Tracker>> _trackers;
    size_t _numberOfTrackers;
    size_t _currentTracker;
    const size_t _FRAMES_TO_LOOSE = 5;              //Limit frames before switch to the next tracker
    size_t _frameCount;                             //Count of frames before switch
    std::vector<bool> _oks;                         //Tracking results for each tracker
    std::vector<std::thread*> _ref_threads;         //Vector of refs to threads for each tracker
    std::vector<cv::Rect2d> _out_boxes;             //Vector of output rects for trackers

    static void Trecker_thread(const size_t i, cv::Mat frame, std::vector<cv::Ptr<cv::Tracker>> &trackers, std::vector<bool> &oks, std::vector<cv::Rect2d> &out_boxes)
    {
        oks[i] = trackers[i]->update(frame, out_boxes[i]);
    }
public:
    MultitrackerKCF(bool withYolo);
    void TrackInitialization(cv::Mat& frame, std::vector<cv::Rect> in_boxes);        //Trackers initialization
    int UpdateTrackers(cv::Mat& frame, bool isDraw);                                 //1: tracking is done 0: tracking can't be done
    std::pair<std::vector<cv::Rect>, size_t> GetTargets();                          //Get vector of target rects + index of the current target
   //<-- ***********************************

   //--> ********** Yolo options ***********
private:
    const std::string _CFG_PATH = "/home/nvidia/Projects/Multitracker/Model/yolov3.cfg";
    const std::string _WEIGHT_PATH = "/home/nvidia/Projects/Multitracker/Model/yolov3.weights";
    bool _withYolo;                                 // Flag if Yolo is ON
    const double _TRESHOLD = 0.8;                   //Probability threshold for Yolo detection
    Detector* _YoloDetector;
    std::vector<bbox_t> _resultsYolo;    
    TrackedObjects _objType;   
    cv::Point centerPoint(cv::Rect2d r);

public:
    void SetTrackedObrjectType(TrackedObjects obj);
    void UpdateTrackers_Yolo(cv::Mat& frame);
    std::vector<cv::Rect> GetYoloRects();


   //<-- ***********************************

};

#endif // MULTITRACKERKCF_H
