#include "MultitrackerKCF.h"

MultitrackerKCF::MultitrackerKCF(bool withYolo)
{
    _withYolo = withYolo;
    if(_withYolo)
    {
        _YoloDetector = new Detector(this->_CFG_PATH, this->_WEIGHT_PATH);
        _objType = Person;
    }
}

 void MultitrackerKCF::TrackInitialization(cv::Mat& frame, std::vector<cv::Rect> in_boxes)
 {
     if (in_boxes.size() == 0) return;
     for (auto box : in_boxes)
     {
         cv::Ptr<cv::Tracker> tracker = cv::TrackerKCF::create();
         tracker->init(frame, box);
         this->_trackers.push_back(tracker);
     }
     this->_numberOfTrackers = _trackers.size();
     this->_oks.resize(this->_numberOfTrackers);
     this->_ref_threads.resize(this->_numberOfTrackers);
     this->_out_boxes.resize(this->_numberOfTrackers);
     this->_currentTracker = 0;
 }

 int MultitrackerKCF::UpdateTrackers(cv::Mat& frame, bool isDraw)
 {
     if (this->_numberOfTrackers == 0) return 0;
     for (size_t i=0; i < this->_numberOfTrackers; ++i)
     {
        this->_ref_threads[i] = new std::thread(MultitrackerKCF::Trecker_thread, i, frame, ref(this->_trackers), ref(this->_oks), ref(this->_out_boxes));
     }

     for (size_t i=0; i < this->_numberOfTrackers; ++i)
     {
         if (this->_ref_threads[i]->joinable()) this->_ref_threads[i]->join();
         if (this->_oks[i])
         {
             if(isDraw)
             {
                 if (i == this->_currentTracker)
                 {
                     cv::rectangle(frame, this->_out_boxes[i], cv::Scalar(0,0,255), 3, 1);
                 }
                 else
                 {
                     rectangle(frame, this->_out_boxes[i], cv::Scalar(0,255,0), 2, 1);
                 }
                 cv::putText(frame, "Id"+std::to_string(i), cv::Point(this->_out_boxes[i].x, this->_out_boxes[i].y-5), cv::FONT_HERSHEY_SIMPLEX, 0.75, cv::Scalar(0,0,255),2);
             }
         }
         else
         {
             if (i == this->_currentTracker)
             {
                 this->_frameCount++;
                 if (this->_frameCount > this->_FRAMES_TO_LOOSE)
                 {
                     this->_frameCount = 0;
                     this->_currentTracker++;
                     if (this->_currentTracker > this->_numberOfTrackers-1)
                     {
                         return 0;
                     }
                 }
             }
             if(isDraw)
             {
                 cv::putText(frame, "Failure tracking Id" + std::to_string(i), cv::Point(150,20*(i+1)), cv::FONT_HERSHEY_SIMPLEX, 0.6, cv::Scalar(0,0,255),2);
             }
         }
     }
     return 1;
 }

 void MultitrackerKCF::UpdateTrackers_Yolo(cv::Mat& frame)
 {
     _resultsYolo = _YoloDetector->detect(frame, this->_TRESHOLD);
     for (auto &r : this->_resultsYolo)
     {
         if(r.prob > _TRESHOLD && r.obj_id == _objType)
         {
             cv::Rect rectYolo (r.x, r.y, r.w, r.h);
             for (size_t i=0; i < this->_numberOfTrackers; ++i)
             {
                 if (rectYolo.contains(centerPoint(this->_out_boxes[i])) && (rectYolo.height < this->_out_boxes[i].height*3))
                 //if (rectYolo.contains(centerPoint(this->_out_boxes[i])))
                 {
                     cv::Ptr<cv::Tracker> tracker = cv::TrackerKCF::create();
                     tracker->init(frame, rectYolo);
                     this->_trackers[i] = tracker;
                 }
             }
         }
     }
 }


 void MultitrackerKCF::SetTrackedObrjectType(TrackedObjects obj)
 {
     this->_objType = obj;
 }

 std::pair<std::vector<cv::Rect>, size_t> MultitrackerKCF::GetTargets()
 {
     std::vector<cv::Rect> targets;
     size_t currentTarget;
     size_t count = 0;
     for (size_t i = 0; i<this->_numberOfTrackers; ++i)
     {
         if (this->_oks[i])
         {
             targets.push_back(cv::Rect(_out_boxes[i]));
             if(i == this->_currentTracker)
             {
                 currentTarget = count;
             }
             count++;
         }
     }
     return std::pair<std::vector<cv::Rect>, size_t>(targets, currentTarget);
 }

 cv::Point MultitrackerKCF::centerPoint(cv::Rect2d r)
 {
     return cv::Point(int(r.x+r.width/2), int(r.y+r.height/2));
 }

 std::vector<cv::Rect> MultitrackerKCF::GetYoloRects()
 {
     std::vector<cv::Rect> rects;
     for (auto &r : this->_resultsYolo)
     {
         if(r.prob > _TRESHOLD && r.obj_id == _objType)
         {
            rects.push_back(cv::Rect (r.x, r.y, r.w, r.h));
         }
     }
     return rects;
 }
