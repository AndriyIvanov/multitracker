#include <iostream>
#include <string>
#include <thread>

#include <opencv2/opencv.hpp>
#include <opencv2/imgproc.hpp>
#include <opencv2/imgcodecs.hpp>
#include <opencv2/tracking.hpp>

#include "MultitrackerKCF.h"

using namespace std;
using namespace cv;

#define SSTR(x) static_cast<std::ostringstream &>((std::ostringstream() << std::dec << x)).str()

//void trecker_thread(const size_t i, Mat frame, vector<Ptr<Tracker>> &trackers, vector<bool> &oks, //vector<Rect2d> &out_bboxes)
//{
//    oks[i] = trackers[i]->update(frame, out_bboxes[i]);
//}

const size_t MAX_FRAMES = 10;
const bool isYolo = true;

int main()
{

    const string pathToVideo ("/home/nvidia/Projects/Multitracker/Data/Run 100 m.mp4");
    //const string pathToVideo ("/home/nvidia/Projects/Multitracker/Data/SelfDriving_Car.mp4");

    // Read video
    VideoCapture video(pathToVideo);

    // Exit if video is not opened
    if(!video.isOpened())
    {
        cout << "Could not read video file" << endl;
        return EXIT_FAILURE;
    }

    // Read first frame
    Mat frame;
    if (!video.read(frame))
    {
        cout << "Cannot read video file" << endl;
        return EXIT_FAILURE;
    }

    MultitrackerKCF tracker(isYolo);

    vector<Rect> in_bboxes;

    bool showCrosshair = true;
    bool fromCenter = false;
    cv::selectROIs("MultiTracker", frame, in_bboxes, showCrosshair, fromCenter);
    if(in_bboxes.size() < 1)
        return 0;

    tracker.TrackInitialization(frame, in_bboxes);

    size_t framesCount = 0;

    while(video.read(frame))
    {
        double timer = (double)getTickCount();

        tracker.UpdateTrackers(frame, true);
        framesCount++;
        if (isYolo && (framesCount > MAX_FRAMES))
        {
            framesCount = 0;
            tracker.UpdateTrackers_Yolo(frame);
        }
//        std::vector<cv::Rect> rectsYolo = tracker.GetYoloRects();
//        for(auto& r : rectsYolo)
//        {
//            rectangle(frame, r, cv::Scalar(255,0,0), 2, 1);
//        }

        float fps = getTickFrequency() / ((double)getTickCount() - timer);

        // Display FPS on frame
        putText(frame, "FPS : " + SSTR(int(fps)), Point(10,50), FONT_HERSHEY_SIMPLEX, 0.75, Scalar(255,0,0), 2);

        // Display frame.
        imshow("MultiTracker", frame);

        // Exit if ESC pressed.
        if(waitKey(1) == 27) break;
    }

    return EXIT_SUCCESS;
}


