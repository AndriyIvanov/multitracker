QT += core
QT -= gui

TARGET = Multitracker
CONFIG += console
CONFIG -= app_bundle
CONFIG += link_pkgconfig

INCLUDEPATH +='/usr/include/aarch64-linux-gnu/qt5/'

QMAKE_CXXFLAGS += -std=c++14

TEMPLATE = app

SOURCES += main.cpp
SOURCES += Sources/MultitrackerKCF.cpp

INCLUDEPATH += ../Multitracker/Headers/

INCLUDEPATH += `pkg-config --cflags opencv`
LIBS += `pkg-config --libs opencv`

INCLUDEPATH += /home/nvidia/Downloads/darknet-master/src/
INCLUDEPATH += /home/nvidia/Downloads/darknet-master/include/
LIBS += -L'/home/nvidia/Downloads/darknet-master' -ldarknet

#INCLUDEPATH += /usr/include/
#LIBS += -L'/usr/lib/' -lboost_program_options -lboost_filesystem -lboost_system -lboost_thread
#LIBS += -L'/usr/lib/x86_64-linux-gnu' -pthread

HEADERS += Headers/MultitrackerKCF.h



